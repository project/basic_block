<?php
namespace Drupal\basic_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a 'Basic' block.
 *
 * @Block(
 *   id = "basic_block",
 *   admin_label = @Translation("Basic block"),
 *   category = @Translation("Content Blocks")
 * )
 */
class BasicBlock extends BlockBase {
    /**
     * {@inheritdoc}
     */
    public function build() {
        $config = $this->getConfiguration();
        $basic_block_title = (isset($config['basic_block_title']) ? $config['basic_block_title'] : '');
        $basic_block_sub_title = (isset($config['basic_block_sub_title']) ? $config['basic_block_sub_title'] : '');
        $basic_block_image = (isset($config['basic_block_image']) ? $config['basic_block_image'] : '');
        if (!empty($basic_block_image)) {
            $load_file = file_load($basic_block_image[0]);
        }
        $block_image = "";
        if ($load_file) {
            $get_file_uri = $load_file->get('uri')->getValue();
            $image_style = \Drupal::entityTypeManager()->getStorage('image_style')->load($config['image_style']);
            $block_image = $image_style->buildUrl($get_file_uri[0]['value']);
        }
        return array(
            '#basic_block_title' => $basic_block_title,
            '#basic_block_sub_title' => $basic_block_sub_title,
            '#basic_block_image' => $block_image,
            '#theme' => 'content_basic_block',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);
        // Get existing configuration.
        $config = $this->getConfiguration();
        $form['basic_block_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Basic Block Title'),
            '#default_value' => isset($config['basic_block_title']) ? $config['basic_block_title'] : '',
        );
        $form['basic_block_sub_title'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Basic Block Sub Title'),
            '#default_value' => isset($config['basic_block_sub_title']) ? $config['basic_block_sub_title'] : '',
        );
        // Load image styles
        $optionlist = ImageStyle::loadMultiple();
        foreach ($optionlist as $key => $value) {
            $options[$key] = $value->getName();
        }
        $form['image_style'] = array(
            '#type' => 'select',
            '#title' => $this->t('Basic Block Image Style'),
            '#options' => $options,
            '#default_value' => isset($config['image_style']) ? $config['image_style'] : '',
        );
        $form['basic_block_image'] = array(
            '#type' => 'managed_file',
            '#title' => $this->t('Basic Block Image'),
            '#default_value' => isset($config['basic_block_image']) ? $config['basic_block_image'] : '',
            '#theme' => 'image_widget',
            '#upload_location' => 'public://images/',
            '#preview_image_style' => 'medium',
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        // Save form fields
        parent::blockSubmit($form, $form_state);
        $values = $form_state->getValues();
        $this->configuration['basic_block_title'] = $values['basic_block_title'];
        $this->configuration['basic_block_sub_title'] = $values['basic_block_sub_title'];
        $this->configuration['image_style'] = $values['image_style'];
        $this->configuration['basic_block_image'] = $values['basic_block_image'];

        $basic_block_image = $values['basic_block_image'];
        // Load image file
        if (!empty($basic_block_image)) {
            $file_load = file_load($basic_block_image[0]);
        }
        // Save image
        if (gettype($file_load) == 'object') {
            $file_load->status = FILE_STATUS_PERMANENT;
            $file_load->save();
            $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()->id());
            $file_load->setOwner($user);
        }
    }
}
